# Ansible Playbooks for automated setup of Void Linux

These playbooks automate the installation of Void Linux in either LXC or
conventional chroot's.

For the **chroot** variant, it is assumed that the resulting setup will be booted
directly, so the environment is being configured for that scenario.

For **LXC** those steps will be skipped and firmware removed. Containers run on
the host's kernel, so local firmware would be redundant. Note that the LXC
variant has yet to be tested.


## Usage

**Warning: Existing data on the configured device will be lost!**

First, configure the "vars" of each file, then run them in this order:

1. `_luks.yaml` - Sets up partitions (skip for LXC installs)
2. `_os.yaml`   - Installs Void Linux

These books have been confirmed to work with
[RackNerd 768 MB KVM VPS](https://my.racknerd.com/aff.php?aff=6424&pid=695)
booted into **Rescue Mode**.
